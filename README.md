op-fan-daemon
=============

A simple, self-contained fan daemon for OpenPOWER systems.

To adjust the fan curves, see include/curve.h

Licensed under the GNU GPL v3.
