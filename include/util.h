/**
 * Copyright 2019 Shawn Anastasio
 *
 * This file is part of op-fan-daemon.
 *
 * op-fan-daemon is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * op-fan-daemon is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with op-fan-daemon.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef OP_FAN_DAEMON_UTIL_H
#define OP_FAN_DAEMON_UTIL_H 1

#include <stdbool.h>
#include <stdlib.h>

#define ARRAY_SIZE(x) (sizeof((x)) / sizeof(*(x)))

typedef void * voidp;

#define vec_template_proto(T) \
typedef int (*T ## _comparator)(T, T); \
struct vec_## T { \
    T *data; \
    size_t size; \
    size_t count; \
    void (*destructor)(T); \
}; \
bool vec_ ## T ## _init(struct vec_ ## T *v, size_t initial_size, void (*destructor)(T)); \
void vec_ ## T ## _push_back(struct vec_ ## T *v, T element); \
T vec_ ## T ## _at(struct vec_ ## T *v, size_t i); \
void vec_ ## T ## _remove(struct vec_ ## T *v, size_t i); \
void vec_ ## T ## _destroy(struct vec_ ## T *v); \
bool vec_ ## T ## _contains(struct vec_ ## T *v, T element, T ## _comparator comparator);

vec_template_proto(voidp)
vec_template_proto(int)

void close_destructor(int fd);

#endif // OP_FAN_DAEMON_UTIL_H
