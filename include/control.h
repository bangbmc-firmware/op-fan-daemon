/**
 * Copyright 2019 Shawn Anastasio
 *
 * This file is part of op-fan-daemon.
 *
 * op-fan-daemon is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * op-fan-daemon is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with op-fan-daemon.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef OP_FAN_DAEMON_CONTROL_H
#define OP_FAN_DAEMON_CONTROL_H 1

#include <stdbool.h>
#include <stdint.h>

#include "fan-daemon.h"

void print_zones(struct controller_state *state);
uint32_t read_temp(int sensor_fd);
bool update_zone(struct controller_state *state, enum zone z);

#endif // OP_FAN_DAEMON_CONTROL_H
