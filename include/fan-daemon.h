/**
 * Copyright 2019 Shawn Anastasio
 *
 * This file is part of op-fan-daemon.
 *
 * op-fan-daemon is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * op-fan-daemon is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with op-fan-daemon.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef OP_FAN_DAEMON_H
#define OP_FAN_DAEMON_H 1

#include <stdint.h>
#include <stdbool.h>

#include <syslog.h>
#include <sys/types.h>
#include <dirent.h>

#include "util.h"
#include "curve.h"

#define REASONABLE_PATH_LEN 1024

// Delay between sensor polls in microseconds
#define POLL_DELAY_US 100000

// For debug

#ifdef NDEBUG
#define dbg_printf(...)
#else
#include <stdio.h>
#define dbg_printf(...) fprintf(stderr, __VA_ARGS__)
#define syslog(x, ...) printf(__VA_ARGS__)
#endif

/**
 * OCC sensors are identified by their FRU (Field Replaceable Unit) number.
 * I don't know where these are documented, but I've figured out the following:
 */
#define FRU_TYPE_CORE    0
#define FRU_TYPE_DIMM    2
#define FRU_TYPE_VRM_VDD 6

/**
 * Fan zones. All fans in a zone run at the same speed.
 */
#define NUM_ZONES 3
enum zone {
    ZONE_CPU0,
    ZONE_CPU1,
    ZONE_CHASSIS
};

/**
 * Platform-dependent
 */

#define NUM_PWM_PROVIDERS 2
extern struct pwm_ops *pwm_providers[NUM_PWM_PROVIDERS];
extern struct pwm_ops *pwm_provider;
void install_pwm_provider(struct pwm_ops *ops);

#define DECLARE_PWM_OPS(name) \
static struct pwm_ops name; \
__attribute__((constructor)) static void install_provider(void) { \
    install_pwm_provider(&name); \
} \
static struct pwm_ops name

// Platform-dependent fan control operations
struct pwm_ops {
    // Determine if this pwm driver can be used
    bool (*probe)(void);

    // Set the fan speed for a given CPU zone.
    void (*set_zone_speed)(enum zone, uint8_t);
};

/**
 * Fan control logic
 */

// A single OCC sensor group
struct sensor_group {
    char dir_path[REASONABLE_PATH_LEN];
    DIR *dir;        // sysfs directory containing sensors
};

// Fan control data for a given zone
struct zone_data {
    struct vec_int sensors;
    const struct fan_curve_point *curve;
    size_t curve_size;
    size_t curve_pos;
    size_t next_curve_pos;
    size_t next_curve_count;
};

// Program state
struct controller_state {
    uint32_t flags;
#define STATE_FLAG_CPU1_PRESENT (1 << 0)

    // Sensor groups
    struct sensor_group occ0;
    struct sensor_group occ1;

    // Zone data
    struct zone_data zones[NUM_ZONES];
};

#endif // OP_FAN_DAEMON_H
