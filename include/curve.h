/**
 * Copyright 2019 Shawn Anastasio
 *
 * This file is part of op-fan-daemon.
 *
 * op-fan-daemon is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * op-fan-daemon is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with op-fan-daemon.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef OP_FAN_DAEMON_CURVE_H
#define OP_FAN_DAEMON_CURVE_H 1

#include <stdint.h>

#include "util.h"

struct fan_curve_point {
    // Trigger temperature in millidegrees celcius
    uint32_t temp;

    // Fan speed (0-255) to set
    uint8_t speed;

    /**
     * Hysteresis value. The temperature has to drop
     * below `temp` - `hysteresis` in order to go back
     * to the previous setting
     */
    uint32_t hysteresis;
};

#ifdef DECLARE_CURVES

/**
 * Fan curves are defined here.
 *
 * The format is described above.
 * Each point contains a minimum temperature threshold at which it triggers,
 * a speed to set when it triggers, and a hysteresis value that the temperature
 * must fall by before a previous point is selected.
 */


#define hist 2500

// Curve used by ZONE_CPU0
static struct fan_curve_point zone_cpu0_curve[] = {
    {30000, 50}, {35000, 65, hist}, {40000, 80, hist}, {50000, 82, 6000}, {64000, 84, 6000}, {70000, 255, hist}
};

// Curve used by ZONE_CPU1
static struct fan_curve_point zone_cpu1_curve[] = {
    {30000, 50}, {35000, 65, hist}, {40000, 80, hist}, {50000, 82, 6000}, {64000, 84, 6000}, {70000, 255, hist}
};

// Curve used by ZONE_CHASSIS
static struct fan_curve_point zone_chassis_curve[] = {
    {30000, 50}, {35000, 65, hist}, {40000, 80, hist}, {50000, 85, hist}, {55000, 90, hist}, {60000, 100, hist}, {65000, 255, hist},
};

#undef hist

#endif

#endif // OP_FAN_DAEMON_CURVE_H
